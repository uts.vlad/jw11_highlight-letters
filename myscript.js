const btns = document.getElementsByClassName('btn');

// const buttons= {
//     enter:  `${btns[0]}`,
//     s:      `${btns[1]}`,
//     e:      `${btns[2]}`,
//     o:      `${btns[3]}`,
//     n:      `${btns[4]}`,
//     l:      `${btns[5]}`,
//     z:      `${btns[6]}`,
// };
//
let funcIsRunning = false;
const hilightLetterTab=(event)=>{
    if (funcIsRunning) return;
    if ( event.key ==='Shift' ) return; // -для событий как по 'keyup', так и по 'keydown"
    if ( event.key ==='Alt' ) return; //  -для событий как по 'keyup', так и по 'keydown"
    if (event.ctrlKey || event.altKey || event.metaKey) return; // - решение ТОЛЬКО для события по 'keydown'

    funcIsRunning = true; // блокируем вызов hilightLetterTab на время, когда мы уже здесь в нем!
    for (let elem of btns) {elem.style.backgroundColor = 'black'}


    switch (event.key) {

        case 'Enter':
            btns[0].style.backgroundColor = "blue";
            break;
        case "S":
        case "s":
            btns[1].style.backgroundColor = "blue";
            break;
        case "E":
        case "e":
            btns[2].style.backgroundColor = "blue";
            break;
        case "O":
        case "o":
            btns[3].style.backgroundColor = "blue";
            break;
        case "N":
        case "n":
            btns[4].style.backgroundColor = "blue";
            break;
        case "L":
        case "l":
            btns[5].style.backgroundColor = "blue";
            break;
        case "Z":
        case "z":
            btns[6].style.backgroundColor = "blue";
            break;
        default:     console.log(event.key); break;

    }
    funcIsRunning = false;
};


document.addEventListener('keyup', hilightLetterTab );
/*обработка по 'keyup' - самая сложная, требует обязательного наличия строк 16 и 17 , а строка 18
* даже не спасает.
* Событие "keydown" - самое правильное решение, как говорят все статьи, т.к. приводит к наименьшему
* числу ошибок.
* Событие keypress  специфично тем, что оно не возвращает управляющие клавиши (ctrl, alt и т.д)
поэтому его можно использовать только при получени символьных и цифровых данных.
*
* Для реализации горячих клавиш, включая сочетания – используйте keydown.
* Если нужен именно символ – используйте keypress.
*  */